#include "pch.h"
#include<stdio.h>
#include<time.h>
#include<stdlib.h>

int getrnum()
{
	int rnum = rand() % 20;
	return rnum;
}

bool core(int rnum)
{
	int num;
	printf("输入你猜的数：");
	scanf_s("%d", &num);
	if (rnum == num)
	{
		printf("你猜对了！\n");
		return true;
	}
	else
	{
		printf("你猜错了！");
		if (num > rnum)
			printf("猜的数太大了\n");
		else
			printf("猜的数太小了\n");
		return false;
	}
}


void m1()
{
	core(getrnum());
}


void m2()
{
	int rnum = getrnum();
	while (1)
	{
		if (core(rnum))
			break;
	}
}


bool core10()
{
	int rnum = getrnum();
	for (int i = 10; i >= 1; i--)
	{
		if (core(rnum))
			return true;
		else
			printf("you still have %d chances\n", i);
	}
	return false;
}


void m3()
{
	core10();
}


void m4()
{
	for (int i = 10; i >= 1; i--)
	{
		printf("%dth ROUND started!\n", i);
		if (core10())
			break;
		else
			printf("%dth ROUND failed!\n", i);
	}
}


int main(int argc, char *argv[])
{

	printf("请选择模式：\n");
	printf("1.只猜一次\n");
	printf("2.猜对为止\n");
	printf("3.猜十次\n");
	printf("4.十个数每个数十次\n");
	int cho;
	scanf_s("%d", &cho);

	srand((unsigned)clock());
	switch (cho)
	{
	case 1:
		m1();
		break;
	case 2:
		m2();
		break;
	case 3:
		m3();
		break;
	case 4:
		m4();
		break;
	default:
		printf("你输的是什么\n");
		break;
	}
	printf("游戏结束\n");
}