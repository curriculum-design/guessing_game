#include "pch.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main()
{
	int b, d=-1;
	srand((unsigned)clock()); //随机数生成种子

	while (1)
	{
		printf("这是一个猜数游戏 范围为1~100 现在请输入您所猜的数！\n");
		int a = rand() % 99 + 1; //给a赋一个随机数值（范围为1~100）
		while (a == d) a = rand() % 99 + 1; //防止再取数时与前一个数相等
		d = a;
		for (int c = 1; c < 11; c++)
		{
			scanf_s("%d", &b);
			if (b == a)
			{
				printf("right!\n");
				break;
			}
			else
			{
				printf("wrong!\n");
				if (b > a)
					printf("你输的大了\n");
				else
					printf("你输的小了\n");
			}
		}
		printf("Your Game is Over! Please go to the new turn!\n");
	}
}

